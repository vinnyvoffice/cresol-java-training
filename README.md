# Treinamento Java 

## Dinâmica

* 15 Exemplos
* 30 Práticas
* 15 Soluções
* *out-of-box*, domínio comum, domínio individual

## Referências Oficiais

* https://docs.oracle.com/en/java/javase/13/
* https://openjdk.java.net/projects/jdk/13/

### Documentação (*API Documentation*)

* https://docs.oracle.com/en/java/javase/13/docs/api/index.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/module-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/io/package-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/lang/package-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/lang/annotation/package-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/nio/file/package-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/time/package-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/time/format/package-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/time/temporal/package-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/util/package-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/util/concurrent/package-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/util/function/package-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/util/regex/package-summary.html
* https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/util/stream/package-summary.html

### Especificação da Linguagem (*Java Language Specification*)

* https://docs.oracle.com/javase/specs/jls/se13/html/index.html

### Certificação 1Z0-808

* https://education.oracle.com/pt_BR/java-se-8-programmer-i/pexam_1Z0-808

### Certificação 1Z0-809

* https://education.oracle.com/pt_BR/java-se-8-programmer-i/pexam_1Z0-809

### Certificação 1Z0-810

* https://education.oracle.com/pt_BR/java-se-8-programmer-i/pexam_1Z0-810

### Certificação 1Z0-815

* https://education.oracle.com/pt_BR/java-se-8-programmer-i/pexam_1Z0-815

### Certificação 1Z0-816

* https://education.oracle.com/pt_BR/java-se-8-programmer-i/pexam_1Z0-816

### Certificação 1Z0-817

* https://education.oracle.com/pt_BR/java-se-8-programmer-i/pexam_1Z0-817

## Domínios

## Comum: *movies, tv shows, video games*

* https://www.imdb.com/chart/moviemeter?ref_=nv_mv_mpm
* https://www.imdb.com/chart/toptv/?ref_=nv_tvv_250
* https://www.imdb.com/search/title/?title_type=feature
* https://www.imdb.com/search/title/?title_type=video_game
* https://www.imdb.com/search/title/?title_type=tv_series

## Facilitador: *basketball games*

* https://www.nba.com/scores#/

## Participante: escolha um site dom assunto do seu interesse

* https://tokyo2020.org/en/
* https://www.emmys.com
* https://oscar.go.com
* https://www.dccomics.com/comics
* https://www.ebay.com
* https://www.billboard.com
* http://www.bbc.co.uk
* https://www.tvguide.com/listings/

## Assuntos

### Java 7

* Uso de texto no switch (*String in switch*)
* Gerenciamento automático de recursos (*Automatic resource management,try-with-resources*)
* Inferência de tipo genérico (*diamond operator <>*)
* Verificações do compilador para uso de argumentos variáveis com genéricos (*@SafeVarargs*)
* Literais para números inteiros binários (*Binary integer literals*)
* Separador de literais numéricas (*Underscores in numeric literals*)
* *Multi-catch and rethrowing exceptions with improved type checking*
* *Fork/Join Framework*
* Nova biblioteca para entrada e saída (NIO.2) 

### Java 8 

* Expressões Lambda (*lambda expressions*)
* Referências a Métodos (*method references*)
* Melhorias em Interfaces (*interface enhancements*)
* Interfaces Funcionais de Propósito Geral (*General Purpose Functional Interfaces*)
* Nova biblioteca para suporte a operações estilo funcional (*Stream API*) 
* Novos métodos em IO e NIO.2
* Novos métodos em Mapas 
* Novos métodos em Random, Scanner, String
* Melhorias em Anotações (*Repeatable Annotations, Type Annotations*)
* Nova biblioteca para datas e horários (*Date and Time API*)
* Suporte a Scripts Javascript (*Nashorn Script Engine*)
* Java Mission Control 
* Base 64
* Nomes de parâmetros nos bytecodes (*parameter names*)

## Encontros

## Encontro 1

* Expressões Lambda (*lambda expressions*)
	* Interfaces Funcionais
	* Corpo de Lambda: Bloco ou Linha
	* Captura de variáveis
	* Classe anônima por Lambda
* Referências a Métodos (*method references*)
	* Métodos de instância
	* Métodos de classe
	* Construtores
	* Outras
* Melhorias em Interfaces (*interface enhancements*)
	* métodos default
	* métodos estáticos
	* sobrescrita 
	* métodos privados 
	
## Encontro 2

* Interfaces Funcionais de Propósito Geral (*General Purpose Functional Interfaces*)
	* Function
	* Predicate
	* Supplier
	* Consumer
	* UnaryOperator
	* ToIntFunction, DoubleFunction 
	* BiConsumer, BinaryOperator
* API para suporte a operações estilo funcional (*Stream API*) 
	* of, map, filter, forEach, peek
	* sum, average, mapTo*, *summaryStatistics
	* Optional, min, max, findFirst, findAny
	* noneMatch, anyMatch, allMatch, sort
	* flatMap, boxed
	* IntStream, range, generate, iterate, skip, limit
	* reduce, collect
	
## Encontro 3

* Novos métodos em IO e NIO.2
	* BufferedReader - lines
	* Files - lines; find, list, walk, newDirectoryStream
* Novos métodos em *Collection Framework*
	* Iterable - forEach
	* Collection - stream, parallelStream
	* List - removeIf, replaceAll, sort, of, copyOf
	* Map - getOrDefault, forEach, replaceAll, compute, computeIfAbsent, computeIfPresent, merge, of, ofEntries, entry, copyOf
* Novos métodos em Random, Scanner, String
	* Random - ints, longs, doubles
	* Scanner - tokens, findAll
	* String - lines, chars, codePoints
* Melhorias em Anotações (*Repeatable Annotations, Type Annotations*)
	* @Repeatable
	* ElementType - TYPE_PARAMETER,TYPE_USE
	
## Encontro 4

* Nova API para Datas e Horários (*Date and Time API*)
	* Instante (*Instant*)
	* Datas e horários locais
		* criação - of, now, parse, from, at
		* consulta - is, get, length, range, query
		* adição e subtração e ajustadores - with, plus, minus
		* formatação - format
	* Período e Duração
		* Period
		* Duration
		* datesUntil
		* until
	* Data e horário com fuso horário
		* ZonedDateTime
		* ZonedId
		* ZonedOffset
	* Interoperabilidade com legado
		* from
		* toLocalDate, toLocalTime
		* toZoneId, toZonedDateTime

* Suporte a Scripts Javascript (*Nashorn Script Engine*)
	* Shell Scripts
	* Aplicações
	* JavaFX
* Java Mission Control 
	* JMX Console
	* Java Flight Recorder
* Base 64
	* *encode*
	* *decode*
* Nomes de parâmetros nos *bytecodes* (*parameter names*)

## Encontro 5

* Uso de texto no switch (*String in switch*)
* Gerenciamento automático de recursos (*Automatic resource management,try-with-resources*)
* Inferência de tipo genérico (*diamond operator <>*)
* Verificações do compilador para uso de argumentos variáveis com genéricos (*@SafeVarargs*)
* Literais para números inteiros binários (*Binary integer literals*)
* Separador de literais numéricas (*Underscores in numeric literals*)
* *Multi-catch and rethrowing exceptions with improved type checking*
* *Fork/Join Framework*
* Nova biblioteca para entrada e saída (NIO.2) 
	* Files - is, exists, size, copy, move, delete, create*, set*, get*
	* Files - readAllLines, readString, readAllBytes, write, writeString
	* Files - newBufferedReader, newBufferedWriter, newInputStream, newOutputStream
	* Files - walkFileTree

