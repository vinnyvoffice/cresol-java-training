package learning.domains.movies;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public final class Movie {

	public final String title;
	public final int year;
	public final int month;
	public final int day;
	public final Genre[] genres;
	public final double usaGross;
	public final boolean estimated;
	public final double budget;
	public final int runtime;
	public final Map<LocalDate, Integer> metascores;
	public final Map<LocalDate, Integer> popularities;
	public final Map<String,String> characters;

	public static Movie of(String title, int year, int month, int day, Genre... genres) {
		return new Movie(title, year, month, day, 0.0, false, 0, 0.0, new HashMap<>(), new HashMap<>(), new HashMap<>(),
				genres);
	}

	public static Movie of(String title, int year, int month, int day, double budget, boolean estimated, int runtime,
			double usaGross, Genre... genres) {
		return new Movie(title, year, month, day, budget, estimated, runtime, usaGross, new HashMap<>(),
				new HashMap<>(), new HashMap<>(), genres);
	}

	@Override
	public String toString() {
		return "Movie [title=" + title + ", year=" + year + "]";
	}

	private Movie(String title, int year, int month, int day, double budget, boolean estimated, int runtime,
			double usaGross, Map<LocalDate, Integer> metascores, Map<LocalDate, Integer> popularities, Map<String,String> characters,
			Genre... genres) {
		this.title = title;
		this.year = year;
		this.month = month;
		this.day = day;
		this.budget = budget;
		this.estimated = estimated;
		this.runtime = runtime;
		this.usaGross = usaGross;
		this.metascores = metascores;
		this.popularities = popularities;
		this.characters = characters;
		this.genres = genres;
	}

	public Movie withBudget(double budget, boolean estimated) {
		return Movie.of(title, year, month, day, budget, estimated, runtime, usaGross, genres);
	}
	
	public Movie withUSAGross(double gross) {
		return Movie.of(title, year, month, day, budget, estimated, runtime, gross, genres);
	}
	
	public Movie withRuntime(int runtime) {
		return Movie.of(title, year, month, day, budget, estimated, runtime, usaGross, genres);
	}

	public Movie withMetacore(LocalDate date, Integer metascore) {
		return withMetacore(Map.entry(date, metascore));
	}

	public Movie withMetacore(Map.Entry<LocalDate, Integer> metascore) {
		Map<LocalDate, Integer> copy = new HashMap<>();
		copy.put(metascore.getKey(), metascore.getValue());
		return new Movie(title, year, month, day, budget, estimated, runtime, usaGross, copy, popularities, characters,
				genres);
	}

	public Movie withPopularity(LocalDate date, Integer popularity) {
		return withPopularity(Map.entry(date, popularity));
	}

	public Movie withPopularity(Map.Entry<LocalDate, Integer> popularity) {
		Map<LocalDate, Integer> copy = new HashMap<>();
		copy.put(popularity.getKey(), popularity.getValue());
		return new Movie(title, year, month, day, budget, estimated, runtime, usaGross, metascores, copy, characters, genres);
	}
	
	public Movie withCharacter(String actor, String character) {
		Map<String, String> copy = new HashMap<>();
		copy.put(actor, character);
		return new Movie(title, year, month, day, budget, estimated, runtime, usaGross, metascores, popularities, copy, genres);
	}

	public double getBudget() {
		return budget;
	}

	public LocalDate getReleasedDate() {
		return LocalDate.of(year, month, day);
	}

	public static final Movie.Builder builder(String title) {
		return new Movie.Builder(title);
	}

	public static class Builder {

		private String title;
		private int year;
		private int month;
		private int day;
		private Genre[] genres;
		private double usaGross;
		private boolean estimated;
		private double budget;
		private int runtime;

		Builder(String title) {
			this.title = title;
		}

		public final Builder releasedDate(int year, int month, int day) {
			this.year = year;
			this.month = month;
			this.day = day;
			return this;
		}

		public final Builder usaGross(double usaGross, boolean estimated) {
			this.usaGross = usaGross;
			this.estimated = estimated;
			return this;
		}

		public final Builder budget(double budget) {
			this.budget = budget;
			return this;
		}

		public final Builder runtime(int runtime) {
			this.runtime = runtime;
			return this;
		}

		public final Builder genres(Genre... genres) {
			this.genres = genres;
			return this;
		}

		public final Movie build() {
			return Movie.of(title, year, month, day, budget, estimated, runtime, usaGross, genres);
		}

	}

}