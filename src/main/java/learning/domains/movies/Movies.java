package learning.domains.movies;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import learning.functions.arrays.FirstLast;

public class Movies {

	private final Collection<Movie> collection;

	public static Movies of(Movie... movies) {
		return new Movies(movies);
	}

	public static Movies marvelCinematicUniverse() {
	    	return Movies.of(
	    			Movie.of("Iron Man", 2008, 5, 2,Genre.ACTION, Genre.ADVENTURE, Genre.SCIFI)
	    					.withBudget(140_000_000, true)
	    					.withRuntime(126)
	    					.withUSAGross(318_604_126)
	    					.withCharacter("Robert Downer Jr.","Tony Stark")
	    					.withCharacter("Gwyneth Paltrow","Pepper Potts")
	    					.withCharacter("Jeff Bridges","Obadiah Stane")
	    					.withCharacter("Terrence Howard","Rhodey")
	    					.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),79))
	    					.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),316)),
	    			Movie.of("The Incredible Hulk", 2008, 6, 13, Genre.ACTION, Genre.ADVENTURE, Genre.SCIFI)
							.withBudget(150_000_000, true)
							.withRuntime(135)
							.withUSAGross(134_806_913)
							.withCharacter("Edward Norton","Bruce Banner")
							.withCharacter("Liv Tyler","Betty Ross")
							.withCharacter("Tim Roth","Emil Blonsky")
							.withCharacter("William Hurt","General 'Thunderbolt' Ross")
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),61))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),953)),
	    			Movie.of("Iron Man 2", 2010, 5, 7)
							.withBudget(200_000_000, true)
							.withRuntime(124)
							.withUSAGross(312_433_331)
	    					.withCharacter("Robert Downer Jr.","Tony Stark")
	    					.withCharacter("Gwyneth Paltrow","Pepper Potts")
	    					.withCharacter("Don Cheadle","Rhodey")
	    					.withCharacter("Scarlett Johansson","Natalie Rushman / Natasha Romanoff")
	    					.withCharacter("Samuel L. Jackson","Nick Fury")
	    					.withCharacter("Clark Gregg","Agent Coulson")
	    					.withCharacter("John Slattery","Howard Stark")
	    					.withCharacter("Paul Bettany","J.A.R.V.I.S (voice)")
	    					.withCharacter("Jon Favreau","Happy Hogan")
						    .withMetacore(Map.entry(LocalDate.of(2019, 9, 28),57))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),837)),
	    			Movie.of("Thor", 2011, 5, 6)
							.withBudget(150_000_000, true)
							.withRuntime(115)
							.withUSAGross(181_030_624)
							.withCharacter("Chris Hemsworth","Thor")
							.withCharacter("Natalie Portman","Jane Foster")
							.withCharacter("Tom Hiddleston","Loki")
							.withCharacter("Anthony Hopkins","Odin")
	    					.withCharacter("Clark Gregg","Agent Coulson")
							.withCharacter("Idris Elba","Heimdall")
							.withCharacter("Rene Russo","Frigga")
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),57))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),656)),
	    			Movie.of("Captain America: The First Avenger", 2011, 7, 22, Genre.ACTION, Genre.ADVENTURE, Genre.SCIFI)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),66))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),478))
							.withCharacter("Chris Evans","Captain America / Steve Rogers")
							.withCharacter("Hayley Atwell","Peggy Carter")
							.withCharacter("Sebastian Stan","James Buchanan 'Bucky' Barnes")
							.withCharacter("Hugo Weaving","Johann Schmidt / Red Skull")
							.withCharacter("Samuel L. Jackson","Nick Fury")
							.withBudget(140_000_000, true)
							.withUSAGross(176_654_505)
							.withRuntime(124),
	    			Movie.of("The Avengers", 2012, 5, 4)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),69))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),337))
							.withCharacter("Robert Downey Jr.","Tony Stark / Iron Man")
							.withCharacter("Chris Evans","Steve Rogers / Captain America")
							.withCharacter("Mark Ruffalo","Bruce Banner / The Hulk")
							.withCharacter("Chris Hemsworth","Thor")
							.withCharacter("Scarlett Johansson","Natasha Romanoff / Black Widow")
							.withCharacter("Jeremy Renner","Clint Barton / Hawkeye")
							.withCharacter("Tom Hiddleston","Loki")
	    					.withCharacter("Clark Gregg","Agent Phil Coulson")
							.withCharacter("Cobie Smulders","Agent Maria Hill")
	    					.withCharacter("Samuel L. Jackson","Nick Fury")
	    					.withCharacter("Gwyneth Paltrow","Pepper Potts")
	    					.withCharacter("Paul Bettany","J.A.R.V.I.S (voice)")
							.withBudget(220_000_000, true)
							.withUSAGross(623_357_910)
							.withRuntime(173),
	    			Movie.of("Iron Man 3", 2013, 5, 3)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),62))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),824))
							.withCharacter("Robert Downey Jr.","Tony Stark")
	    					.withCharacter("Gwyneth Paltrow","Pepper Potts")
	    					.withCharacter("Don Cheadle","Rhodey")
							.withCharacter("Guy Pearce","Aldrich Killian")
	    					.withCharacter("Jon Favreau","Happy Hogan")
							.withCharacter("Ben Kingsley","Trevor Slattery")
	    					.withCharacter("Paul Bettany","J.A.R.V.I.S (voice)")
							.withBudget(200_000_000, true)
							.withUSAGross(409_013_994)
							.withRuntime(195),
	    			Movie.of("Thor: The Dark World", 2013, 11, 8)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),54))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),1005))
							.withCharacter("Chris Hemsworth","Thor")
							.withCharacter("Natalie Portman","Jane Foster")
							.withCharacter("Tom Hiddleston","Loki")
							.withCharacter("Anthony Hopkins","Odin")
							.withCharacter("Jaimie Alexander","Sif")
							.withCharacter("Zachary Levi","Fandral")
							.withCharacter("Idris Elba","Heimdall")
							.withCharacter("Rene Russo","Frigga")
							.withBudget(170_000_000, true)
							.withUSAGross(206_362_140)
							.withRuntime(112),
	    			Movie.of("Captain America: The Winter Soldier", 2014, 1, 1, Genre.ACTION, Genre.ADVENTURE, Genre.SCIFI)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),70))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),692))
							.withCharacter("Chris Evans","Steve Rogers / Captain America")
	    					.withCharacter("Samuel L. Jackson","Nick Fury")
							.withCharacter("Scarlett Johansson","Natasha Romanoff / Black Widow")
							.withCharacter("Robert Redford","Alexander Pierce")
							.withCharacter("Sebastian Stan","Bucky Barnes / Winter Soldier")
							.withCharacter("Anthony Mackie","Sam Wilson / Falcon")
							.withCharacter("Cobie Smulders","Agent Maria Hill")
							.withCharacter("Emily VanCamp","Kate / Agent 13")
							.withCharacter("Hayley Atwell","Peggy Carter")
							.withBudget(170_000_000, true)
							.withUSAGross(259_766_572)
							.withRuntime(136),
	    			Movie.of("Guardians of the Galaxy", 2014, 8, 1, Genre.ACTION, Genre.ADVENTURE, Genre.COMEDY)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),76))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),214))
							.withCharacter("Chris Pratt","Peter Quill")
							.withCharacter("Zoe Saldana","Gamora")
							.withCharacter("Dave Bautista","Drax")
							.withCharacter("Vin Diesel","Groot (voice)")
							.withCharacter("Bradley Cooper","Rocket (voice)")
							.withCharacter("Michael Rooker","Yondu Udonta")
							.withCharacter("Karen Gillan","Nebula")
							.withCharacter("Benicio Del Toro","The Collector")
							.withBudget(170_000_000, true)
							.withUSAGross(333_176_600)
							.withRuntime(121),
	    			Movie.of("Avengers: Age of Ultron", 2015, 5, 1)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),66))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),368))
							.withCharacter("Robert Downey Jr.","Tony Stark / Iron Man")
							.withCharacter("Chris Hemsworth","Thor")
							.withCharacter("Mark Ruffalo","Bruce Banner / The Hulk")
							.withCharacter("Chris Evans","Steve Rogers / Captain America")
							.withCharacter("Scarlett Johansson","Natasha Romanoff / Black Widow")
							.withCharacter("Jeremy Renner","Clint Barton / Hawkeye")
							.withCharacter("James Spader","Ultron")
	    					.withCharacter("Samuel L. Jackson","Nick Fury")
	    					.withCharacter("Don Cheadle","James Rhodes / War Machine")
							.withCharacter("Aaron Taylor-Johnson","Pietro Maximoff / Quicksilver")
							.withCharacter("Elizabeth Olsen","Wanda Maximoff / Scarlet Witch")
	    					.withCharacter("Paul Bettany","J.A.R.V.I.S / Vision")
							.withCharacter("Cobie Smulders","Maria Hill")
							.withCharacter("Anthony Mackie","Sam Wilson / Falcon")
							.withCharacter("Hayley Atwell","Peggy Carter")
							.withBudget(250_000_000, true)
							.withUSAGross(459_005_868)
							.withRuntime(141),
	    			Movie.of("Ant-Man", 2015, 7, 17)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),64))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),1_002))
							.withCharacter("Paul Rudd","Scott Lang / Ant-Man")
							.withCharacter("Michael Douglas","Dr. Hank Pym")
							.withCharacter("Evangeline Lilly","Hope van Dyne")
							.withCharacter("Corey Stoll","Darren Cross / Yellowjacket")
							.withCharacter("Anthony Mackie","Sam Wilson / Falcon")
							.withCharacter("Hayley Atwell","Peggy Carter")
	    					.withCharacter("John Slattery","Howard Stark")
							.withBudget(130_000_000, true)
							.withUSAGross(180_202_163)
							.withRuntime(117),
	    			Movie.of("Captain America: Civil War", 2016, 5, 6)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),75))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),442))
							.withCharacter("Chris Evans","Steve Rogers / Captain America")
							.withCharacter("Robert Downey Jr.","Tony Stark / Iron Man")
							.withCharacter("Scarlett Johansson","Natasha Romanoff / Black Widow")
							.withCharacter("Sebastian Stan","Bucky Barnes / Winter Soldier")
							.withCharacter("Anthony Mackie","Sam Wilson / Falcon")
	    					.withCharacter("Don Cheadle","James Rhodes / War Machine")
							.withCharacter("Jeremy Renner","Clint Barton / Hawkeye")
							.withCharacter("Chadwick Boseman","T'Challa / Black Panther")
							.withCharacter("Paul Bettany","Vision")
							.withCharacter("Elizabeth Olsen","Wanda Maximoff / Scarlet Witch")
							.withCharacter("Paul Rudd","Scott Lang / Ant-Man")
							.withCharacter("Emily VanCamp","Sharon Carter")
							.withCharacter("Tom Holland","Peter Parker / Spider-Man")
							.withBudget(250_000_000, true)
							.withUSAGross(408_084_349)
							.withRuntime(147),
	    			Movie.of("Doctor Strange", 2016, 11, 4)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),72))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),580))
							.withCharacter("Benedict Cumberbatch","Dr. Stephen Strange")
							.withCharacter("Chiwetel Ejiofor","Mordo")
							.withCharacter("Rachel MacAdams","Dr. Christine Palmer")
							.withCharacter("Benedict Wong","Wong")
							.withCharacter("Mads Mikkelsen","Kaecilius")
							.withCharacter("Tilda Swinton","The Ancient One")
							.withBudget(165_000_000, true)
							.withUSAGross(232_641_920)
							.withRuntime(115),
	    			Movie.of("Guardians of the Galaxy Vol. 2", 2017, 5, 5)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),67))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),393))
							.withCharacter("Chris Pratt","Peter Quill / Star-Lord")
							.withCharacter("Zoe Saldana","Gamora")
							.withCharacter("Dave Bautista","Drax")
							.withCharacter("Vin Diesel","Baby Groot (voice)")
							.withCharacter("Bradley Cooper","Rocket (voice)")
							.withCharacter("Michael Rooker","Yondu")
							.withCharacter("Karen Gillan","Nebula")
							.withCharacter("Pom Kelmentieff","Mantis")
							.withCharacter("Sylvester Stallone","Stakar Ogord")
							.withCharacter("Kurt Russel","Ego")
							.withBudget(200_000_000, true)
							.withUSAGross(389_813_101)
							.withRuntime(136),
	    			Movie.of("Spider-Man: Homecoming", 2017, 7, 7)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),73))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),111))
							.withCharacter("Tom Holland","Peter Parker / Spider-Man")
							.withCharacter("Michael Keaton","Adrian Toomes / Vulture")
							.withCharacter("Robert Downey Jr.","Tony Stark / Iron Man")
							.withCharacter("Marisa Tomei","May Parker")
							.withCharacter("Jon Favreau","Happy Hogan")
							.withCharacter("Gwyneth Paltrow","Petter Potts")
							.withCharacter("Zendaya","Michelle")
							.withBudget(175_000_000, true)
							.withUSAGross(334_201_140)
							.withRuntime(133),
	    			Movie.of("Thor: Ragnarok", 2017, 11, 3, Genre.ACTION, Genre.ADVENTURE, Genre.COMEDY)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),74))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),156))
							.withCharacter("Chris Hemsworth","Thor")
							.withCharacter("Tom Hiddleston","Loki")
							.withCharacter("Cate Blanchett","Hela")
							.withCharacter("Idris Elba","Heimdall")
							.withCharacter("Jeff Goldblum","Grandmaster")
							.withCharacter("Tessa Thompson","Valkyrie")
							.withCharacter("Karl Urban","Skurge")
							.withCharacter("Mark Ruffalo","Bruce Banner / Hulk")
							.withCharacter("Anthony Hopkins","Odin")
							.withCharacter("Benedith Cumberbatch","Doctor Strange")
							.withBudget(180_000_000, true)
							.withUSAGross(315_058_289)
							.withRuntime(130),
	    			Movie.of("Black Panther", 2018, 2, 16)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),88))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),194))
							.withCharacter("Chadwick Boseman","T'Challa / Black Panther")
							.withCharacter("Michael B. Jordan","Erik Killmonger")
							.withCharacter("Lupita Nyong'o","Nakia")
							.withCharacter("Danai Gurira","Okoye")
							.withCharacter("Martin Freeman","Everett K. Ross")
							.withCharacter("Daniel Kaluuya","W'Kabi")
							.withCharacter("Letitia Wright","Shuri")
							.withCharacter("Angela Bassett","Ramonda")
							.withCharacter("Forest Whitaker","Zuri")
							.withBudget(200_000_000, true)
							.withUSAGross(700_059_566)
							.withRuntime(134),
	    			Movie.of("Avengers: Infinity War", 2018, 4, 27)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),68))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),94))
							.withCharacter("Robert Downey Jr.","Tony Stark / Iron Man")
							.withCharacter("Chris Hemsworth","Thor")
							.withCharacter("Mark Ruffalo","Bruce Banner / Hulk")
							.withCharacter("Chris Evans","Steve Rogers / Captain America")
							.withCharacter("Scarlett Johansson","Natasha Romanoff / Black Widow")
	    					.withCharacter("Don Cheadle","James Rhodes / War Machine")
							.withCharacter("Benedict Cumberbatch","Dr. Stephen Strange")
							.withCharacter("Tom Holland","Peter Parker / Spider-Man")
							.withCharacter("Chadwick Boseman","T'Challa / Black Panther")
							.withCharacter("Zoe Saldana","Gamora")
							.withCharacter("Karen Gillan","Nebula")
							.withCharacter("Tom Hiddleston","Loki")
	    					.withCharacter("Paul Bettany","Vision")
							.withCharacter("Elizabeth Olsen","Wanda Maximoff / Scarlet Witch")
							.withCharacter("Anthony Mackie","Sam Wilson / Falcon")
							.withCharacter("Sebastian Stan","Bucky Barnes / Winter Soldier")
							.withCharacter("Idris Elba","Heimdall")
							.withCharacter("Danai Gurira","Okoye")
							.withCharacter("Peter Dinklage","Eitri")
							.withCharacter("Benedict Wong","Wong")
							.withCharacter("Pom Kelmentieff","Mantis")
							.withCharacter("Dave Bautista","Drax")
							.withCharacter("Vin Diesel","Baby Groot (voice)")
							.withCharacter("Bradley Cooper","Rocket (voice)")
							.withCharacter("Gwyneth Paltrow","Petter Potts")
							.withCharacter("Benicio Del Toro","The Collector")
							.withCharacter("Josh Brolin","Thanos")
							.withCharacter("Chris Pratt","Peter Quill / Star-Lord")
							.withCharacter("William Hurt","Secretary of State Thaddeus Ross")
							.withCharacter("Letitia Wright","Shuri")
							.withBudget(321_000_000, true)
							.withUSAGross(678_815_482)
							.withRuntime(149),
	    			Movie.of("Ant-Man and the Wasp", 2018, 7, 6)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),70))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),632))
							.withCharacter("Paul Rudd","Scott Lang / Ant-Man")
							.withCharacter("Evangeline Lilly","Hope Van Dyne / Wasp")
							.withCharacter("Hannah John-Kamen","Ava / Ghost")
							.withCharacter("Abby Ryder Fortson","Cassie")
							.withCharacter("Michelle Pfeiffer","Janet Van Dyne / Wasp")
							.withCharacter("Laurence Fishburne","Dr. Bill Foster")
							.withCharacter("Michael Douglas","Dr. Hank Pym")
							.withBudget(162_000_000, true)
							.withUSAGross(216_648_740)
							.withRuntime(118),
	    			Movie.of("Captain Marvel", 2019, 1, 1)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),64))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withBudget(00_000_000, true)
							.withUSAGross(426_000_000)
							.withRuntime(123),
	    			Movie.of("Avengers: Endgame", 2019, 1, 1, Genre.ACTION, Genre.ADVENTURE, Genre.SCIFI)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),78))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withBudget(00_000_000, true)
							.withUSAGross(858_000_000)
							.withRuntime(181),
	    			Movie.of("Spider-Man: Far from Home", 2019, 1, 1)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),69))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withBudget(00_000_000, true)
							.withUSAGross(388_000_000)
							.withRuntime(129),
	    			Movie.of("Guardians of the Galaxy Vol. 3", 2021, 1, 1)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withBudget(00_000_000, true)
							.withUSAGross(000_000_000)
							.withRuntime(0),
	    			Movie.of("Black Widow", 2020, 1, 1)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withBudget(00_000_000, true)
							.withUSAGross(000_000_000)
							.withRuntime(0),
			    	Movie.of("Eternals", 2020, 1, 1)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withBudget(00_000_000, true)
							.withUSAGross(000_000_000)
							.withRuntime(0),
			    	Movie.of("Untitled Marvel Film", 2020, 1, 1)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withBudget(00_000_000, true)
							.withUSAGross(000_000_000)
							.withRuntime(0),
			    	Movie.of("Shang-Chi and the Legend of the Ten Rings", 2021, 1, 1)
							.withMetacore(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withPopularity(Map.entry(LocalDate.of(2019, 9, 28),0))
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withCharacter("...","...")
							.withBudget(00_000_000, true)
							.withUSAGross(000_000_000)
							.withRuntime(0)
	    	);
	    }

	private Movies(Movie... movies) {
		this.collection = Arrays.asList(movies);
	}

	public Collection<Movie> selectAll() {
		return collection;
	}

	public Collection<String> selectTitles() {
		Stream<Movie> stream = collection.stream();
		Stream<String> titles = stream.map(m -> m.title);
		return titles.collect(Collectors.toList());
	}

	public FirstLast<Integer> selectDistinctYears() {
		return () -> collection.stream().map(m -> m.year).distinct().toArray(Integer[]::new);
	}

	public Collection<String> whereYearIs(int year) {
		return collection.stream().filter(m -> m.year == year).map(m -> m.title).collect(Collectors.toList());
	}

	public Collection<String> whereTitleContains(String text) {
		return collection.stream().filter(m -> m.title.contains(text)).map(m -> m.title).collect(Collectors.toList());
	}

	public Optional<Movie> selectMaxYear(int year) {
		return collection.stream().filter(m -> m.year >= year).max((m1, m2) -> Integer.compare(m1.runtime, m2.runtime));
	}

	public Optional<Movie> selectMinYear(int runtime) {
		return collection.stream().filter(m -> m.runtime >= runtime).min((m1, m2) -> Integer.compare(m1.runtime, m2.runtime));
	}

	public Map<Genre, List<Movie>> groupByGenre() {
		//TODO 
		return collection.stream().collect(Collectors.groupingBy(m -> m.genres[0]));
	}

	public Map<Boolean, List<Movie>> havingRuntimeGreaterThan(int minutes) {
		return collection.stream().collect(Collectors.partitioningBy(m -> m.runtime > minutes));
	}

	public Collection<String> whereGenreIs(Genre genre) {
		return collection.stream().filter(m -> Arrays.asList(m.genres).contains(genre)).map(m -> m.title)
				.collect(Collectors.toList());
	}

	public BigDecimal sumBudget() {
		return collection.stream().map(m -> BigDecimal.valueOf(m.budget)).reduce((b1, b2) -> b1.add(b2))
				.orElse(BigDecimal.ZERO);
	}

}
