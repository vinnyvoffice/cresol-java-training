package learning.domain.basketball;

import java.util.function.Function;
import java.util.function.Predicate;

import learning.domain.basketball.GameFunctions.Scorer;

/**
 * @see https://stats.nba.com/schedule/
 */
public class Game {

	public final String away, home;
	public final int awayPoints, homePoints;
	
	public Game(String away, String home, int awayPoints, int homePoints) {
		super();
		this.away = away;
		this.home = home;
		this.awayPoints = awayPoints;
		this.homePoints = homePoints;
	}
	
	public boolean isAwayWinner() {
		return this.awayPoints > this.homePoints;
	}
	
	public boolean is(Predicate<Game> predicate) {
		return predicate.test(this);
	}
	
	public String getScorex() {
		Scorer scorer = g -> 
		g.away + " "+g.awayPoints + " x " + g.homePoints + " " + g.home;
		scorer = g -> 
		g.away + " "+g.awayPoints + "," + " " + g.home + " "+g.homePoints;

		return scorer.calculate(this);
	}

	final Function<Game,String> defaultTemplate = g -> 
	g.away + " "+g.awayPoints + "," + " " + g.home + " "+g.homePoints;

	public String getScore() {
		return getScore(defaultTemplate);
	}

	public final String getScore(Function<Game,String> scorer) {
		return scorer.apply(this);
	}
	
	
}
