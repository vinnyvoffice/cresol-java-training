package learning.domain.basketball;

public class InterfaceEnhancements {

	public static void main(String[] args) {
		InterfaceEnhancements.C c = new InterfaceEnhancements.C();
		c.m1();
	}
	
	interface A {
		default void m1() { System.out.println("A");}
	}
	interface B {
		void m1();
	}
	
	interface D extends B {
		default void m1() { System.out.println("D");}
	}
	
	static class C implements A, D {

		@Override
		public void m1() {
			D.super.m1();
		}

		

		
	}

}
