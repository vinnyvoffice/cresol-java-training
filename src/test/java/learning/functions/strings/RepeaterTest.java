package learning.functions.strings;

import java.util.function.Predicate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RepeaterTest  {

	
	private static final int COUNT = 3;
	private static final String LAMBDA = "lambda";
	private static final String LAMBDALAMBDALAMBDA = "lambdalambdalambda";

	@Test
	public void repeat5() {
		Repeater repeater = Repeater::usingVersion5;
		Assertions.assertEquals(LAMBDALAMBDALAMBDA, repeater.repeat(LAMBDA, COUNT));
	}

	
	@Test
	public void repeat8() {
		Repeater repeater = Repeater::usingVersion8;
		Assertions.assertEquals(LAMBDALAMBDALAMBDA, repeater.repeat(LAMBDA, COUNT));
	}

	
	@Test
	public void repeat11() {
		Repeater repeater = Repeater::usingVersion11;
		Assertions.assertEquals(LAMBDALAMBDALAMBDA, repeater.repeat(LAMBDA, COUNT));
	}

	@Test
	public void sameResult() {
		Repeater repeater5 = Repeater::usingVersion5;
		Repeater repeater8 = Repeater::usingVersion8;
		Repeater repeater11 = Repeater::usingVersion11;
		Predicate<String> predicate5 = (s) -> s.equals(repeater5.repeat(LAMBDA, COUNT));
		Predicate<String> predicate8 = (s) -> s.equals(repeater8.repeat(LAMBDA, COUNT));
		Predicate<String> predicate11 = (s) -> s.equals(repeater11.repeat(LAMBDA, COUNT));
		Predicate<String> predicateAll = predicate5.and(predicate8).and(predicate11);
		Assertions.assertTrue(predicateAll.test(LAMBDALAMBDALAMBDA));
	}
	
}
