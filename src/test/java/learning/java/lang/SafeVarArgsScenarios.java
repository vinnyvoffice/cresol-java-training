package learning.java.lang;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SafeVarArgsScenarios {

	@Test
	public void safevarargs() {
		List<String> list = Arrays.asList("abc");
		try {
			faultyMethodStatic(list);
			faultyMethodFinal(list);
			faultyMethodPrivate(list);
		} catch(ClassCastException e) {
			Assertions.assertTrue(true);
		}
	}

	@SafeVarargs
	public static String faultyMethodStatic(List<String>... l) {
		Object[] objectArray = l; // Valid
		objectArray[0] = Arrays.asList(42);
		return l[0].get(0); // ClassCastException thrown here
	}

	@SafeVarargs
	public final String faultyMethodFinal(List<String>... l) {
		Object[] objectArray = l; // Valid
		objectArray[0] = Arrays.asList(42);
		return l[0].get(0); // ClassCastException thrown here
	}

	@SafeVarargs
	private String faultyMethodPrivate(List<String>... l) {
		Object[] objectArray = l; // Valid
		objectArray[0] = Arrays.asList(42);
		return l[0].get(0); // ClassCastException thrown here
	}
	
	
	 @SuppressWarnings("unchecked")
	String faultyMethodDefault(List<String>... l) {
		Object[] objectArray = l; // Valid
		objectArray[0] = Arrays.asList(42);
		return l[0].get(0); // ClassCastException thrown here
	}
	 
}
