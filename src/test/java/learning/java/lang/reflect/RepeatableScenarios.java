package learning.java.lang.reflect;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Target;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


@Task("mostrar exemplo de uso do Repeatable")
@Task("mostrar exemplo de uso do Repeatable")
public class RepeatableScenarios {

	@Test
	public void recommended() {
		@Recommended("use sempre com ....")
		String expected = "X";
		String actual = "x".toUpperCase();
		Assertions.assertEquals(expected, actual);
	}
	
	public @Recommended("Recomendado uso de texto para cados ont") Object xxx() {
		return "xxx";
	}
	
}

@Repeatable(TaskList.class)
@interface Task {
	String value();
}

@Documented
@Target(ElementType.TYPE_USE)
@interface Recommended {
	String value();
}

@interface TaskList{
	Task[] value();
}