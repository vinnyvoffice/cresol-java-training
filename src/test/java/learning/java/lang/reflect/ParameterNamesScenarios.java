package learning.java.lang.reflect;

import java.lang.reflect.Method;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParameterNamesScenarios {
	
	@Test
	public void parameter_name_success() throws NoSuchMethodException, SecurityException {
		Method method = ParameterNamesScenarios.class.getMethod("demo", String.class);
		String name = method.getParameters()[0].getName();
		Assertions.assertEquals("qualquer", name);
	}
	
	public void demo(String qualquer) {}
}

