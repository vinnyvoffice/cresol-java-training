package learning.java.grammar;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UnderscoreScenarios {

	@Test
	public void underscore() {
		double gross = 100000000.0_0;
		gross = 0_7;
		// gross = 0b_0;
		gross = 0x1_F;
		Assertions.assertTrue(gross > 0);
	}
	
}
