package learning.java.grammar;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BinaryLiteralsScenarios {

	@Test
	public void binary_literals() {
		int value = 07;
		value = 8;
		value = 0xF;
		value = 0b100;
		Assertions.assertEquals(4, value);
	}
	
}
