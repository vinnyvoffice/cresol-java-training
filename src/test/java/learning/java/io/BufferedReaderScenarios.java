package learning.java.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BufferedReaderScenarios {

	@Test
	public void lines8() {
		try (BufferedReader reader = new BufferedReader(
						new FileReader("./src/test/java/learning/java/io/BufferedReaderScenarios.java"));
				Stream<String> lines = reader.lines();) {
			Assertions.assertEquals("package learning.java.io;", lines.findFirst().orElse(""));
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

	@Test
	public void lines() {
		BufferedReader reader = null;
		Stream<String> lines = null;
		try {

			reader = new BufferedReader(
					new FileReader("./src/test/java/learning/java/io/BufferedReaderScenarios.java"));
			lines = reader.lines();
			Assertions.assertEquals("package learning.java.io;", lines.findFirst().orElse(""));
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		} finally {
			if (lines != null)
				lines.close();
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
		}
	}
}
