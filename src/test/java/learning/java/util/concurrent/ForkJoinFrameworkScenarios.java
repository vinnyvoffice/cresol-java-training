package learning.java.util.concurrent;

import java.io.PrintStream;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.function.Function;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ForkJoinFrameworkScenarios {

	@Test
	public void max_parallel() {
		max_value_1_000_1_100(true);
	}

	@Test
	public void max_sequential() {
		max_value_1_000_1_100(false);
	}

	void max_value_1_000_1_100(boolean parallel) {
		ForkJoinPool pool = ForkJoinPool.commonPool();
		int[] data = new Random().ints(1_000_000, 1, 101).toArray();
		RecursiveTask<Integer> task = new MaxValueFinder(parallel, data);
		Integer max = pool.invoke(task);
		Assertions.assertEquals(100, max);
	}

}

class MaxValueFinder extends RecursiveTask<Integer> {

	private final PrintStream logger = System.out;
	private static final long serialVersionUID = 1L;
	private int[] data;
	private int start, end;
	private final boolean parallel;
	private static final int batchSize = 100;

	MaxValueFinder(boolean parallel, int[] data) {
		this(parallel, data, 0, data.length - 1);
	}

	MaxValueFinder(boolean parallel, int[] data, int start, int end) {
		this.parallel = parallel;
		this.data = data;
		this.start = start;
		this.end = end;
	}

	Function<String, Function<String, String>> func = a -> b -> "";

	@Override
	protected Integer compute() {
		if (deveQuebrar()) {
			logger.printf("breaking (%s,%s) %n", start, end);
			int middle = (end - start) / 2 + start;
			MaxValueFinder t1 = new MaxValueFinder(parallel, data, start, middle);
			MaxValueFinder t2 = new MaxValueFinder(parallel, data, middle + 1, end);
			if (parallel) {
				t1.fork();
				return Math.max(t1.join(), t2.compute());
			} else {
				return Math.max(t1.compute(), t2.compute());
			}
		} else {
			logger.printf("solving (%s,%s) %n", start, end);
			return IntStream.rangeClosed(start, end).map(i -> data[i]).max().orElse(Integer.MIN_VALUE);
		}
	}

	boolean deveQuebrar() {
		return (end - start) > batchSize;
	}
}
