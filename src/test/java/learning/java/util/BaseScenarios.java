package learning.java.util;

import java.util.Base64;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BaseScenarios {

	@Test
	public void example1() {
		byte[] encoded = Base64.getEncoder().encode(new String("qualquer").getBytes());
		Assertions.assertEquals("qualquer", new String(Base64.getDecoder().decode(encoded)));
	}
}
