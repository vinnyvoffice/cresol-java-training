package learning.java.util;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MapScenarios {

	@Test
	public void compute() {
		Map<String,Integer> map = new HashMap<>();
		BiFunction<? super String, ? super Integer, ? extends Integer> remappingFunction = (k,v) -> v;
		String key = "SC";
		map.compute(key , remappingFunction);
	}
	
	@Test
	public void ofEntries() {
		Map<String,Integer> map = Map.ofEntries(Map.entry("SC", 1),Map.entry("PR", 1),Map.entry("RS", 1));
		Assertions.assertNotNull(map);
	}
}
