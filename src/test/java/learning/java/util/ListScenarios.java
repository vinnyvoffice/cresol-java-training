package learning.java.util;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ListScenarios {

	@Test
	public void replaceAll() {
		List<Integer> list = Arrays.asList(1,3,4,5,5,6);
		list.replaceAll(n -> n*2);
		Assertions.assertEquals(2, list.get(0));
	}
	
	@Test
	public void sort() {
		List<Integer> list = Arrays.asList(6,1,3,4,5,5);
		list.sort((n1,n2) -> n1 - n2);
		Assertions.assertEquals(1, list.get(0));
	}
	
	@Test
	public void of() {
		List<Integer> list = List.of();
		Assertions.assertThrows(UnsupportedOperationException.class, () -> list.sort((n1,n2) -> n1 - n2));
	}
	
	/**
	 * @see java.util.List#copyOf(java.util.Collection)
	 */
	@Test
	public void copyOf() {
		List<Integer> list = List.copyOf(Arrays.asList(1,3));
		Assertions.assertThrows(UnsupportedOperationException.class, () -> list.sort((n1,n2) -> n1 - n2));
	}
}
