package learning.java.nio.file;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SwitchScenarios {

	@Test
	public void switch_with_string() {
		boolean result;
		String genre = "ACTION";
		switch (genre) {
		case "ACTION":
		case "ADVENTURE":
		case "SCIFI":
			result = true;
			break;
		default:
			result = false;
			break;
		}
		Assertions.assertEquals(true, result);
	}
	
}
