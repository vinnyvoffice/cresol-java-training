package learning.java.nio.file;

import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TryWithResourcesCatchThrowScenarios {

	@Test()
	public void try_with_resources() throws Exception {
		final List<String> messages = new LinkedList<>();
		AutoCloseable resource1 = () -> {
			messages.add("resource1 closed");
			throw new IllegalArgumentException("erro ao fechar resource 1");
		};
		AutoCloseable resource2 = () -> System.out.println("resource2 closed");
		try (AutoCloseable resource3 = resource1; AutoCloseable resource4 = resource2) {// FIXME java 8 limitation
			messages.add("resource 1 used");
			if (true) {
				messages.add("resource 2 used");
				throw new IllegalStateException("Deu erro");
			}
		} catch (IllegalStateException e) {
			messages.add(e.getLocalizedMessage());
			for (Throwable t: e.getSuppressed()) {
				messages.add(t.getLocalizedMessage());
			}
		}
		Assertions.assertEquals(5, messages.size());
	}

	@Test
	public void multicatch() {
		try {
			new File("/multicatch.txt").createNewFile();
		} catch (IllegalStateException | IOException e) {
			Assertions.assertTrue(true);
		}
	}

	@Test
	public void retrowing() throws IOException, SQLException {
		try {
			try {
				new File("/multicatch.txt").createNewFile();
				DriverManager.getConnection(null);
			} catch (Exception e) {
				throw e;
			}
		} catch (IOException | SQLException e) {
			Assertions.assertTrue(true);
		}
	}
	
}
