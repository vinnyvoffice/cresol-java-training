package learning.java.nio.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FilesScenarios {

	final String directory = "./src/test/java";
	final String thisFile = "./src/test/java/learning/java/nio/file/FilesScenarios.java";
	final String fileToWrite = "./src/test/java/learning/java/nio/file/writable.txt";
	final String directoryToCreate = "./src/test/java/learning/java/nio/file/created";
	final String moviesCsv = "./src/test/java/learning/java/nio/file/movies.csv";
	/**
	 * @throws IOException
	 * @see java.io.File
	 * @see java.nio.file.Files#lines(java.nio.file.Path)
	 * @see java.nio.file.Path
	 * @see java.nio.file.Paths
	 */
	@Test
	void lines() throws IOException {
		Stream<String> lines = Files.lines(Paths.get(moviesCsv));
		long expected = 25;
		long actual = lines.peek(System.out::println).count();
		lines.close();
		Assertions.assertEquals(expected, actual);
	}

	/**
	 * @throws IOException 
	 * @see java.nio.file.Files#find(Path, int, java.util.function.BiPredicate,
	 *      FileVisitOption...)Path, int, java.util.function.BiPredicate,
	 *      FileVisitOption...)Path, int, java.util.function.BiPredicate,
	 *      FileVisitOption...)
	 * 
	 */
	@Test
	void find() throws IOException {
		long actual = Files.find(Paths.get(directory),5, (p,a) -> a.isRegularFile()).count();
		Assertions.assertEquals(25, actual);
	}

	/**
	 * @see java.nio.file.Files#newDirectoryStream(Path)
	 * @see java.nio.file.Files#list(Path)
	 */
	@Test
	void list() throws IOException {
		long actual = Files.list(Paths.get(directory)).count();
		Assertions.assertEquals(2, actual);
	}

	@Test
	void walk() throws IOException {
		Path start = Paths.get(directory);
		FileVisitOption options = FileVisitOption.FOLLOW_LINKS;
		try (Stream<Path> walk = Files.walk(start, options);) {
			long actual = walk.map(p -> p.toString()).peek(System.out::println).count();
			Assertions.assertEquals(48, actual);
		}
	}

	/**
	 * @see java.nio.file.Files#newDirectoryStream(Path)
	 * @see java.nio.file.Files#list(Path)
	 */
	@Test
	void newDirectoryStream() throws IOException {
		DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(directory));
		int counter = 0;
		for (@SuppressWarnings("unused") Path path : directoryStream)
			counter++;
		Assertions.assertEquals(2, counter);
	}

	@Test
	public void readFile() {
		File file = new File(thisFile);
		// Assertions.assertEquals("",file.getAbsolutePath());
		Assertions.assertTrue(file.exists());
		// java.nio.file.Files
		// java.nio.file.Path
		// java.nio.file.Paths
		Assertions.assertTrue(Files.exists(Paths.get(thisFile)));
	}

	@Test
	public void createDirectory() throws IOException {
		File file = new File(directoryToCreate);
		Assertions.assertTrue(file.mkdir());
		file.delete();
		Path path = Paths.get(directoryToCreate);

		Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rwxr-x---");
		FileAttribute<Set<PosixFilePermission>> attrs = PosixFilePermissions.asFileAttribute(perms);
		Assertions.assertTrue(Files.createDirectory(path, attrs) != null);
		Files.delete(path);
	}

	@Test
	public void readAllLines() throws IOException {
		Path path = Paths.get(thisFile);
		List<String> lines = Files.readAllLines(path);
		Assertions.assertTrue(lines.size() > 0);
	}

	@Test
	public void writeLines() throws IOException {
		List<String> lines = Arrays.asList("line1\\nline2\\line3");
		Path created = Files.write(Paths.get(fileToWrite), lines);
		Assertions.assertTrue(Files.exists(created));
	}

}
