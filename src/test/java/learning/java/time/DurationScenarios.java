package learning.java.time;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DurationScenarios {

	@Test
	public void period() {
		LocalDate expected = LocalDate.parse("2019-09-25");
		Period period = Period.of(0, 14, 3);
		LocalDate actual = expected.plus(period);
		System.out.println(period);
		Assertions.assertEquals(LocalDate.parse("2020-11-28"), actual);

	}
	
	@Test
	public void period_between() {
		Period between = Period.between(LocalDate.of(2019, 1, 1), (LocalDate.of(2019, 3, 1)));
		Assertions.assertEquals(2, between.getMonths());
	}
	
	@Test
	public void duration() {
		LocalTime expected = LocalTime.parse("20:30");
		Duration duration = Duration.ofHours(5).plusMinutes(30);
		LocalTime actual = expected.plus(duration);
		Assertions.assertEquals(LocalTime.parse("02:00"), actual);

	}
	
	@Test
	public void duration_between() {
		Duration between = Duration.between(LocalTime.of(20,00), (LocalTime.of(23,30)));
		Assertions.assertEquals(12600, between.getSeconds());
	}
}
