package learning.java.time;

import javax.script.Compilable;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.junit.jupiter.api.Test;

public class ScriptEngineScenarios {

	
	@Test
	public void xxx() throws ScriptException, NoSuchMethodException {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("nashorn");
		Object eval = engine.eval(" function upper(str) { return str.toUpperCase(); } upper('teste'); ");
		System.out.println(eval);
		if (engine instanceof Compilable) {
			Compilable compilable = (Compilable) engine;
		}
		if (engine instanceof Invocable) {
			Invocable invocable = (Invocable) engine;
			Object result = invocable.invokeFunction("upper", "cresol");
			System.out.println(result);
		}
	}
}
