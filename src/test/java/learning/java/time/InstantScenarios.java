package learning.java.time;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class InstantScenarios {

	@Test
	public void until() {
		long millis = Instant.ofEpochMilli(100).until(Instant.ofEpochMilli(250), ChronoUnit.MILLIS);
		Assertions.assertEquals(150, millis);
	}
	
	@Test
	public void until_elapsed_time() {
		Instant start = Instant.ofEpochMilli(100);
		Instant end = Instant.ofEpochMilli(250);
		long nanos = start.until(end, ChronoUnit.NANOS);
		Assertions.assertEquals(150_000000, nanos);
	}
}
