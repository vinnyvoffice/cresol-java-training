package learning.java.time;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.util.Date;
import java.util.Locale;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LocalDateScenarios {

	@Test
	public void of() {
		LocalDate actual = LocalDate.of(2019, Month.SEPTEMBER, 25);
		Assertions.assertEquals(2019, actual.getYear());
		Assertions.assertEquals(9, actual.getMonthValue());
		Assertions.assertEquals(25, actual.getDayOfMonth());
	}

	@Test
	public void at() {
		LocalDateTime expected = LocalDateTime.of(2019, Month.SEPTEMBER, 24, 22, 00);
		LocalDate reference = LocalDate.of(2019, Month.SEPTEMBER, 25);
		LocalDateTime datetime = reference.atTime(LocalTime.of(10, 00));
		ZonedDateTime japan = datetime.atZone(ZoneId.of("Japan"));
		ZonedDateTime east = japan.withZoneSameInstant(ZoneId.of("Brazil/East"));
		LocalDateTime actual = east.toLocalDateTime();
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void with() {
		TemporalAdjuster adjuster = d -> d.plus(3, ChronoUnit.DAYS);
		LocalDate expected = LocalDate.of(2019, 9, 28);
		LocalDate actual = LocalDate.of(2019, Month.SEPTEMBER, 25).with(adjuster);
		Assertions.assertEquals(expected, actual);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void from() {
		Date actual = Date.from(Instant.ofEpochMilli(0));
		Assertions.assertEquals(69, actual.getYear());
	}

	@Test
	public void datesUntil() {
		LocalDate expected = LocalDate.of(2019, Month.SEPTEMBER, 25);
		Stream<LocalDate> stream = expected.datesUntil(LocalDate.of(2019, 12, 25));
		Assertions.assertEquals(91, stream.count());
	}

	@Test
	public void plus() {
		LocalDate expected = LocalDate.of(2019, Month.DECEMBER, 25);
		LocalDate actual = LocalDate.of(2019, Month.AUGUST, 25).plusMonths(4);
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void now() {
		// ZoneId.getAvailableZoneIds().stream().filter(z ->
		// !z.contains("Brazil")).forEach(System.out::println);
		Assertions.assertTrue(LocalDate.now().isAfter(LocalDate.of(2019, Month.SEPTEMBER, 25)));
		Assertions.assertTrue(LocalDate.now(Clock.system(ZoneId.of("Japan"))).isAfter(LocalDate.of(2019, 9, 25)));
		Assertions.assertTrue(LocalTime.now(Clock.system(ZoneId.of("Europe/Berlin"))).isAfter(LocalTime.of(01, 00)));
	}

	@Test
	public void parse() {
		LocalDate expected = LocalDate.parse("2019-09-25");
		LocalDate actual = LocalDate.parse("2019::09::25", DateTimeFormatter.ofPattern("yyyy::MM::dd"));
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void is_get() {
		LocalDate expected = LocalDate.parse("2019-09-25");
		expected.getDayOfWeek();
		expected.isLeapYear();
		LocalDate actual = LocalDate.parse("2019::09::25", DateTimeFormatter.ofPattern("yyyy::MM::dd"));
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void length() {
		LocalDate expected = LocalDate.parse("2019-09-25");
		expected.lengthOfMonth();
		expected.lengthOfYear();
		LocalDate actual = LocalDate.parse("2019::09::25", DateTimeFormatter.ofPattern("yyyy::MM::dd"));
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void format() {
		String monthOnly = LocalDate.parse("2019-09-25")
				.format(DateTimeFormatter.ofPattern("MMMM", new Locale("pt", "BR")));
		Assertions.assertEquals("setembro", monthOnly);
		LocalDate custom = LocalDate.parse("2019::09::25", DateTimeFormatter.ofPattern("yyyy::MM::dd"));
		Assertions.assertEquals(LocalDate.of(2019, 9, 25), custom);
	}
}
