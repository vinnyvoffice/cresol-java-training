package learning.domains.movies;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import learning.util.testing.SystemOutRule;

public class MoviesTest {

	final Movies movies = Movies.of(
			Movie.of("Ghost",1990, 12, 1, Genre.DRAMA, Genre.FANTASY, Genre.ROMANCE)
			     .withRuntime(127)
			     .withBudget(22_000_000, true)
			     .withUSAGross(217_631_306),
			Movie.of("Black Panther",2015, 1, 20, Genre.ACTION, Genre.ADVENTURE, Genre.SCIFI)
			     .withRuntime(134)
			     .withBudget(200_000_000, true)
			     .withUSAGross(700_059_566)
	);
	
	final SystemOutRule rule = new SystemOutRule();
	
	@BeforeEach
	public void out() {
		rule.enableLog();
	}
	
	@Test
	public void selectTitles() {
		String[] expected = new String[] {"Ghost","Black Panther"};
		Assertions.assertEquals(Arrays.asList(expected), movies.selectTitles());
	}
	
	@Test
    public void selectDistinctYears() {
    	Assertions.assertEquals(1990, movies.selectDistinctYears().first());
    	Assertions.assertEquals(2015, movies.selectDistinctYears().last());
    }
    
	@Test
    public void whereYearIs() {
    	Assertions.assertEquals(1, movies.whereYearIs(2015).size());
    }
    
	@Test
    public void whereTitleContains() {
    	Assertions.assertEquals(1, movies.whereTitleContains("Black").size());
    }
    
	@Test
    public void selectMaxYear() {
    	Assertions.assertEquals(true, movies.selectMaxYear(2015).isPresent());
    }
    
	@Test
    public void selectMinYear() {
    	Assertions.assertEquals(true, movies.selectMaxYear(1981).isPresent());
    }
    
	@Test
    public void groupByGenre() {
    	Assertions.assertEquals(1,movies.groupByGenre().get(Genre.ACTION).size());
    }
    
	@Test
    public void havingRuntimeGreaterThan() {
    	Assertions.assertEquals(2,movies.havingRuntimeGreaterThan(100).size());
    }
    
	@Test
    public void whereGenreIs() {
    	Assertions.assertEquals(1,movies.whereGenreIs(Genre.ACTION).size());
    }
    
	@Test
    public void sumBudget() {
    	Assertions.assertEquals(222_000_000.0,movies.sumBudget().doubleValue());
    }

	
	/**********/
    
	@Test
	public void showTitles() {
		Collection<String> titles = movies.selectTitles();
		titles.stream().forEach(System.out::println);
		Assertions.assertEquals("Ghost\nBlack Panther\n", rule.getLog());
	}
	
	@Test void localDate() {
		List<Movie> moviesAfter2010 = movies.selectAll().stream().filter(m -> m.getReleasedDate().isAfter(LocalDate.of(2010, 1, 1))).collect(Collectors.toList());
		moviesAfter2010.forEach(System.out::println);
		Assertions.assertEquals("Movie [title=Black Panther, year=2015]\n", rule.getLog());
	}
	
	@Test
	@DisplayName("Define a pipeline using of, map, filter, forEach, peek functional operations")
	public void forEach() {
		//Movie[] array = movies.selectAll().toArray(Movie[]::new);
		Stream<Movie> stream = movies.selectAll().stream();
		//stream = Stream.of(array);
		stream
			.peek(System.out::println)
			.filter(m -> m.year > 1990)
			.peek(System.out::println)
			.map(m -> m.title)
			.forEach(t -> System.out.print(t));
		//TODO
		Assertions.assertEquals("Movie [title=Ghost, year=1990]\n" + 
				"Movie [title=Black Panther, year=2015]\n" + 
				"Movie [title=Black Panther, year=2015]\n" + 
				"Black Panther", rule.getLog());
	}
	
	@Test
	public void sum() {
		int expected = 4005;
		int actual = movies.selectAll().stream()
			  .mapToInt(m -> m.year)
			  .sum();
		Assertions.assertEquals(expected, actual);
	}
	
	@Test
	public void average() {
		double expected = 2002.5;
		double actual = movies.selectAll().stream()
			  .mapToInt(m -> m.year)
			  .average().orElse(0);
		Assertions.assertEquals(expected, actual);
	}
	
	@Test
	public void summaryStats() {
		IntSummaryStatistics summaryStatistics = movies.selectAll().stream()
			  .mapToInt(m -> m.year)
			  .summaryStatistics();
		Assertions.assertEquals(2, summaryStatistics.getCount());
		Assertions.assertEquals(2002.5, summaryStatistics.getAverage());
		Assertions.assertEquals(1990, summaryStatistics.getMin());
		Assertions.assertEquals(2015, summaryStatistics.getMax());
		Assertions.assertEquals(4005, summaryStatistics.getSum());
	}
	
	@Test
	public void min() {
		double expected = 1990;
		double actual = movies.selectAll().stream()
			  .mapToInt(m -> m.year)
			  .min().orElseThrow(IllegalArgumentException::new);
		Assertions.assertEquals(expected, actual);
	}
	
	@Test
	public void optional1() {
		String title = null;
		Optional.ofNullable(title).ifPresent(t -> System.out.println(t));
		Assertions.assertEquals("",rule.getLog());
	}
	
	@Test
	public void optional2() {
		String title = "Jurassic World";
		Optional.ofNullable(title).ifPresent(t -> System.out.println(t));
		Assertions.assertEquals("Jurassic World\n",rule.getLog());
	}
	
	@Test
	public void findFirst() {
		Optional<Movie> first = movies.selectAll().stream().findFirst();
		Assertions.assertNotNull(first);
	}
	
	@Test
	public void findAny() {
		Optional<Movie> any = movies.selectAll().stream().findAny();
		Assertions.assertNotNull(any);

	}
	
	@Test
	public void noneMatch() {
		boolean actual = movies.selectAll()
				.stream()
				.noneMatch(m -> m.title.startsWith("Gh"));
		Assertions.assertFalse(actual);
	}
	
	@Test
	public void allMatch() {
		boolean actual = movies.selectAll()
				.stream()
				.allMatch(m -> m.title.startsWith("Gh"));
		Assertions.assertFalse(actual);
	}
	
	@Test
	public void anyMatch() {
		boolean actual = movies.selectAll()
				.stream()
				.anyMatch(m -> m.title.startsWith("Gh"));
		Assertions.assertTrue(actual);
	}
	
	@Test
	public void range() {
		IntStream.range(0, 10).sum();
		IntStream.rangeClosed(1, 10).count();
	}
	
	@Test
	public void generate() {
		Random random = new Random();
		IntStream stream = IntStream.generate(() -> random.nextInt());
		Assertions.assertEquals(100,stream.skip(3).limit(100).count());
	}
	
	@Test
	public void iterate() {
		IntStream stream = IntStream.iterate(0, i -> i*i);
		Assertions.assertEquals(100,stream.skip(3).limit(100).count());
	}
	
	@Test public void flatMap() {
		Genre[] genres = movies.selectAll()
			.stream()
			.flatMap(m -> Stream.of(m.genres))
			.distinct()
			.sorted()
			.toArray(Genre[]::new);
		Assertions.assertEquals("[SCIFI, ROMANCE, ACTION, DRAMA, ADVENTURE, FANTASY]", Arrays.toString(genres));
	}
	
	@Test
	public void collect() {
		//ToDoubleFunction<Movie> mapper = Movie::getBudget;
		String result = movies.selectAll()
				.stream()
				//.collect(Collectors.toCollection(LinkedList::new));
				//.collect(Collectors.averagingDouble(mapper));
				.map(m -> m.title)
				.collect(Collectors.joining("::"));
		Assertions.assertEquals("Ghost::Black Panther", result);
		
	}
	
	@Test
	public void reduce() {
		Optional<String> result = movies.selectAll()
		.stream()
		.map(m -> m.title)
		.reduce((t1,t2) -> t1.concat("::").concat(t2));
		Assertions.assertEquals("Ghost::Black Panther", result.orElse(""));
	}
	
	
}
